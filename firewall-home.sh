#!/bin/sh
####################################
#Script iptable for home please edit or add for other solution (vpn ....)
#By Aurelien Husson
# https://relhost.net
####################################

PATH=/bin:/sbin:/usr/bin:/usr/sbin

echo "*completely emptying the rules"
iptables -t filter -F
iptables -t filter -X

echo "*We block traffic"
iptables -t filter -P INPUT DROP
iptables -t filter -P FORWARD DROP
iptables -t filter -P OUTPUT ACCEPT

echo "*except the already established connections"
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

echo "*We allow the localhost to communicate with itself (the local loop)"
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A OUTPUT -o lo -j ACCEPT

echo "*Allow ping both ways"
iptables -t filter -A INPUT -p icmp -j ACCEPT

echo "*Authorization SSH traffic entering for the management server"
iptables -t filter -A INPUT -p tcp --dport 22 -j ACCEPT


echo "*Open intranet for samba"
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p tcp --dport 135 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 135 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p tcp --dport 137 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 137 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p tcp --dport 138 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 138 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p tcp --dport 139 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 139 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p tcp --dport 445 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 445 -j ACCEPT

echo "*Open intranet for plex media server"
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p tcp --dport 32400 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 32400 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 32410 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 32412 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 32414 -j ACCEPT
iptables -A INPUT -s 192.168.1.0/24 -m state --state NEW -p udp --dport 1900 -j ACCEPT
iptables -t filter -A INPUT -p tcp --dport 32400 -j ACCEPT
iptables -t filter -A INPUT -p udp --dport 32400 -j ACCEPT

#echo "*Open port for syncthing"
#iptables -t filter -A INPUT -p tcp --dport 8384 -j ACCEPT
#iptables -t filter -A INPUT -p tcp --dport 22000 -j ACCEPT

#echo "*Open port for torrent"
#iptables -t filter -A INPUT -p tcp --dport 49154:65535 -j ACCEPT
#iptables -t filter -A INPUT -p udp --dport 49154:65535 -j ACCEPT
#iptables -t filter -A INPUT -p tcp --dport 51413 -j ACCEPT
#iptables -t filter -A INPUT -p udp --dport 51413 -j ACCEPT

#echo "*Open vpn port"
#echo 1 > /proc/sys/net/ipv4/ip_forward
#iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
#iptables -A FORWARD -i tun0 -o eth0 -j ACCEPT

#echo "*Open GIT"
#iptables -t filter -A INPUT -p tcp --dport 9418 -j ACCEPT
#iptables -t filter -A INPUT -p tcp --dport 9418 -j ACCEPT


echo "*Open port for IMAP SMTP POP3"
iptables -t filter -A INPUT -p tcp --dport 25 -j ACCEPT
iptables -t filter -A INPUT -p tcp --dport 465 -j ACCEPT
iptables -t filter -A INPUT -p tcp --dport 587 -j ACCEPT

echo "*Open port for NTP, DNS,"
iptables -t filter -A INPUT -p tcp --dport 123 -j ACCEPT
iptables -t filter -A INPUT -p tcp --dport 53 -j ACCEPT
iptables -t filter -A INPUT -p udp --dport 53 -j ACCEPT

echo "*Protection DDOS"
iptables -A FORWARD -p tcp --syn -m limit --limit 1/second -j ACCEPT
iptables -A FORWARD -p udp -m limit --limit 1/second -j ACCEPT
iptables -A FORWARD -p icmp --icmp-type echo-request -m limit --limit 1/second -j ACCEPT

echo "*Anti Scan port"
iptables -A FORWARD -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s -j ACCEPT

echo "*CHAIN "SYN"
iptables -N SYN
iptables -A SYN -m limit --limit 10/min --limit-burst 3 -j LOG --log-level warning --log-prefix "[firewall] [SYN: DROP]"
iptables -A SYN -m limit --limit 10/min --limit-burst 3 -m recent --set --name SYN-DROP -j DROP
iptables -A SYN -m limit --limit 1/min --limit-burst 1 -j LOG --log-level warning --log-prefix "[firewall] [SYN: FLOOD!]"
iptables -A SYN -j DROP

echo "*CHAIN "SCANNER"
iptables -N SCANNER
iptables -A SCANNER -m limit --limit 10/min --limit-burst 3 -j LOG --log-level warning --log-prefix "[firewall] [SCANNER: DROP]"
iptables -A SCANNER -m limit --limit 10/min --limit-burst 3 -m recent --set --name SUSPEITO -j DROP
iptables -A SCANNER -m limit --limit 1/min --limit-burst 1 -j LOG --log-level warning --log-prefix "[firewall] [SCANNER: FLOOD!]"
iptables -A SCANNER -j DROP

