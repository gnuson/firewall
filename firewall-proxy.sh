#!/bin/sh
####################################
# Script ip6table for proxy squid
# By Aurelien Husson
# https://relhost.net
####################################


FW_ACTIVE=yes
FW_MASQ=yes
IFACE_INET=eth0
IFACE_LAN=eth1
IFACE_LAN_IP=192.168.2.0/24

IPT=$(which iptables)
SYS=$(which sysctl)


echo "Block Input"
$IPT -P INPUT DROP
$IPT -P FORWARD ACCEPT
$IPT -P OUTPUT ACCEPT

echo "Loopack")
$IPT -A INPUT -i lo -j ACCEPT

echo "accept ping"
$IPT -A INPUT -p icmp -j ACCEPT

echo "accept igmp"
$IPT -A INPUT -p igmp -j ACCEPT

echo "Keep already connection"
$IPT -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

# echo "FTP port"
# /sbin/modprobe ip_conntrack_ftp
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 21 -j ACCEPT

echo "Open SSH"
$IPT -A INPUT -p tcp -i $IFACE_LAN --dport 22 -j ACCEPT
# $IPT -A INPUT -p tcp -i $IFACE_INET --dport 22 -m state --state NEW \
# -m recent --set --name SSH
# $IPT -A INPUT -p tcp -i $IFACE_INET --dport 22 -m state --state NEW \
#  -m recent --update --seconds 60 --hitcount 2 --rttl --name SSH -j DROP
# $IPT -A INPUT -p tcp -i $IFACE_INET --dport 22 -j ACCEPT

# echo "Open DNS"
# $IPT -A INPUT -p udp -i $IFACE_LAN --dport 53 -j ACCEPT
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 53 -j ACCEPT

# echo "Open DHCP"
# $IPT -A INPUT -p udp -i $IFACE_LAN --dport 67 -j ACCEPT

# echo "Open HTTP"
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 80 -j ACCEPT

# echo "Open NTP"
# $IPT -A INPUT -p udp -i $IFACE_LAN --dport 123 -j ACCEPT

# echo "Open CUPS"
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 631 -j ACCEPT
# $IPT -A INPUT -p udp -i $IFACE_LAN --dport 631 -j ACCEPT

# echo "Open Samba"
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 135 -j ACCEPT
# $IPT -A INPUT -p udp -i $IFACE_LAN --dport 137 -j ACCEPT
# $IPT -A INPUT -p udp -i $IFACE_LAN --dport 138 -j ACCEPT
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 139 -j ACCEPT
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 445 -j ACCEPT

# echo "Open NFS"
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 111 -j ACCEPT
# $IPT -A INPUT -p udp -i $IFACE_LAN --dport 111 -j ACCEPT
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 2049 -j ACCEPT
# $IPT -A INPUT -p udp -i $IFACE_LAN --dport 2049 -j ACCEPT
# $IPT -A INPUT -p tcp -i $IFACE_LAN --dport 32765:32769 -j ACCEPT
# $IPT -A INPUT -p udp -i $IFACE_LAN --dport 32765:32769 -j ACCEPT

echo "Open Squid"
$IPT -A INPUT -p tcp -i $IFACE_LAN --dport 3128 -j ACCEPT
$IPT -A INPUT -p udp -i $IFACE_LAN --dport 3128 -j ACCEPT
$IPT -t nat -A PREROUTING -i $IFACE_LAN -p tcp ! -d 192.168.2.1 \
--dport 80 -j REDIRECT --to-port 3128

echo "Apt-Cacher"
$IPT -A INPUT -p tcp -i $IFACE_LAN --dport 3142 -j ACCEPT

echo "Log"
$IPT -A INPUT -j LOG --log-prefix "+++ IPv4 packet rejected +++ "
$IPT -A INPUT -j REJECT


if [ $FW_MASQ = 'yes' ]; then
$IPT -t nat -A POSTROUTING -o $IFACE_INET -s $IFACE_LAN_IP -j MASQUERADE
$SYS -q -w net.ipv4.ip_forward=1
fi
