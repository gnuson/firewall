#!/bin/sh
####################################
# Script ip6table fpr server
# By Aurelien Husson
# https://relhost.net
####################################

echo "We drop INPUT"
ip6tables -t filter -P INPUT DROP
ip6tables -t filter -P FORWARD DROP
ip6tables -t filter -P OUTPUT ACCEPT

echo "Allow loopack"
ip6tables -A INPUT -i lo -j ACCEPT


echo "Keep already connection"
ip6tables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT


echo "broadcast"
ip6tables -A INPUT -p icmpv6 --icmpv6-type neighbour-solicitation -m hl --hl-eq 255 -j ACCEPT
ip6tables -A INPUT -p icmpv6 --icmpv6-type neighbour-advertisement -m hl --hl-eq 255 -j ACCEPT
ip6tables -A INPUT -p icmpv6 --icmpv6-type router-advertisement -m hl --hl-eq 255 -j ACCEPT


echo "ping"
ip6tables -A INPUT -p icmpv6 --icmpv6-type echo-request -m conntrack --ctstate NEW -m limit --limit 1/s --limit-burst 1 -j ACCEPT

echo "Port open"
ip6tables -A INPUT -p tcp --dport 80 -j ACCEPT
ip6tables -A INPUT -p tcp --dport 443 -j ACCEPT

echo "Protect chain"
ip6tables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
ip6tables -A INPUT -p tcp --tcp-flags SYN,FIN SYN,FIN -j DROP
ip6tables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
ip6tables -A INPUT -p tcp --tcp-flags FIN,RST FIN,RST -j DROP
ip6tables -A INPUT -p tcp --tcp-flags ACK,FIN FIN -j DROP
ip6tables -A INPUT -p tcp --tcp-flags ACK,PSH PSH -j DROP
ip6tables -A INPUT -p tcp --tcp-flags ACK,URG URG -j DROP
